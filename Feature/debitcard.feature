# new feature
# Tags: optional

Feature: Validate ordering the Debit card feature

  Background: test

    Given I setup appium for iOS App - debit card


  Scenario: I validate ordering of debit card if KYC is enabled - Premium
    When I click on CL Cards tab - debit card
    Then I click to Order the card
    Then I choose Premium option
    And I agree on terms and condition
    And I Choose currency as GBP
    And I verify the address
    Then I Validate My name on The Card
    And I see congratulations Message
    And I exit the screen
    When I click on Activate the card
    Then I provide Valid CVV number
    And Card should be Activated

  Scenario: I validate ordering of debit card if KYC is enabled - Standard
    When I click on CL Cards tab - debit card
    Then I click to Order the card
    Then I choose Standard option
    And I agree on terms and condition
    And I Choose currency as GBP
    And I verify the address
    Then I Validate My name on The Card
    And I see congratulations Message
    And I exit the screen
    When I click on Activate the card
    Then I provide Valid CVV number
    And Card should be Activated


  Scenario: Activate the debit card with correct CVV

    When I click on Activate the card

    Then I provide Valid CVV number

    Then I click on Confirm card Number

    Then I see sucess Message for card Activation

    And Card should be Activated


  Scenario: Activate the debit card with Incorrect CVV

    When I click on Activate the card

    Then I provide InValid CVV number

    Then I click on Confirm card Number

    And Wrong CVV message should be displayed


  Scenario: Validate max invalid CVV numver
    When I click on Activate the card

    Then I provide InValid CVV number

    And Wrong CVV message should be displayed
    When I click on Activate the card
    Then I click on Confirm card Number

    Then I provide InValid CVV number

    And Wrong CVV message should be displayed
    When I click on Activate the card
    Then I click on Confirm card Number

    Then I provide InValid CVV number

    And Wrong CVV message should be displayed
    When I click on Activate the card
    Then I click on Confirm card Number

    Then I provide InValid CVV number

    And Wrong CVV message should be displayed
    When I click on Activate the card
    Then I click on Confirm card Number

    Then I provide InValid CVV number

    And Debit card Lock Message should be deisplayed

  Scenario: validate Entering special charactes in CVV

  Scenario: Activate the debit card with Incorrect CVV

    When I click on Activate the card

    Then I provide spacial characters in CVV number

    And Wrong CVV message should be displayed

  Scenario: Validate error message if Activate the card without CVV

    When I click on Activate the card

    Then I click on Confirm card Number

    And It show error message for blank CVV





