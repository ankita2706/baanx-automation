# new feature

Feature: I Validate multiple Actions in Baanx App

  Background: I setup appium configuration for iOS app.

    Given I setup appium for iOS App

#Adding Wallet
#CryptoWallet
  Scenario: I verify adding new crypto - bitcoin cash wallet

    When I scroll down to see the bottom of the App

    When I click on + button

    Then I add bitcoin cash wallet BCH

    Then I click on confirm



  Scenario: I verify adding new crypto - Litcoin LTC wallet

    When I scroll down to see the bottom of the App

    When I click on + button

    Then I add Litcoin LTC wallet

    Then I click on confirm



  Scenario: I verify adding new crypto - Bitcoin BTC wallet

    When I scroll down to see the bottom of the App

    When I click on + button

    Then I add Bitcoin wallet BTC

    Then I click on confirm


#FIAT Currency wallet - when KYC is done

  Scenario: I verify adding new FIAT Currency wallet - USD -  KYC Done

    When I scroll down to see the bottom of the App

    Then I click on + button

    Then I click on FIAT Currency Wallet type

    And I click on USD currency wallet

    Then I click on confirm



  Scenario: I verify adding new FIAT Currency wallet - Euro - KYC Done

    Then I scroll down to see the bottom of the App

    Then I click on + button

    Then I click on FIAT Currency Wallet type

    And I click on Euro Currency Wallet

    Then I click on confirm




  Scenario: I verify adding new FIAT Currency wallet - British Currency - KYC Done

    Then I scroll down to see the bottom of the App

    Then I click on + button

    Then I click on FIAT Currency Wallet type

    And I click on British Currency Wallet Type

    Then I click on confirm



#CryptoAdvanceWallet
#  Scenario: I verify adding new crypto Advance - bitcoin cash wallet
#
#    When I scroll down to see the bottom of the App
#
#    When I click on + button
#
#    Then I click on Crypto Advance wallet
#
#    Then I add bitcoin cash Advance wallet BCH
#
#    Then I click on confirm
#
#  Scenario: I verify adding new crypto Advance - Litcoin LTC Advance wallet
#
#    When I scroll down to see the bottom of the App
#
#    When I click on + button
#
#    Then I click on Crypto Advance wallet
#
#    Then I add Litcoin LTC Advance wallet
#
#    Then I click on confirm
#
#  Scenario: I verify adding new crypto Advance - Bitcoin wallet
#
#    When I scroll down to see the bottom of the App
#
#    When I click on + button
#
#    Then I click on Crypto Advance wallet
#
#    Then I add Bitcoin wallet Advance BTC
#
#    Then I click on confirm

#BXCoin
  Scenario: I verify adding BX coin

    When I scroll down to see the bottom of the App

    When I click on + button

    Then I click on Token

    Then I select wallet currency for BX

    Then I click on confirm



#Adding Wallet - If wallet already added
#CryptoWallet

  Scenario: I verify adding same crypto - bitcoin cash wallet

    When I scroll down to see the bottom of the App

    When I click on + button

    Then I add bitcoin cash wallet BCH

    Then I click on confirm

    And Wallet Already Added error shows up

    And I click on OK

    And I Check Wallet Name as BCH

  Scenario: I verify adding same crypto - Litcoin LTC wallet

    When I scroll down to see the bottom of the App

    When I click on + button

    Then I add Litcoin LTC wallet

    Then I click on confirm

    And Wallet Already Added error shows up

    And I click on OK

    And I Check Wallet Name as LTC



  Scenario: I verify adding new crypto - Bitcoin BTC wallet

    When I scroll down to see the bottom of the App

    When I click on + button

    Then I add Bitcoin wallet BTC

    Then I click on confirm

    And Wallet Already Added error shows up
    And I click on OK
    And I Check Wallet Name as BTC




#FIAT Currency wallet
  Scenario: I verify adding same FIAT Currency wallet - USD

    When I scroll down to see the bottom of the App

    Then I click on + button

    Then I click on FIAT Currency Wallet type

    And I click on USD currency wallet

    Then I click on confirm

    And Wallet Already Added error shows up

    And I click on OK
    And I Check Wallet Name as USD



  Scenario: I verify adding same FIAT Currency wallet - Euro

    Then I scroll down to see the bottom of the App

    Then I click on + button

    Then I click on FIAT Currency Wallet type

    And I click on Euro Currency Wallet

    Then I click on confirm
    And Wallet Already Added error shows up
    And I click on OK
    And I Check Wallet Name as EUR



  Scenario: I verify adding same FIAT Currency wallet - British Currency

    Then I scroll down to see the bottom of the App

    Then I click on + button

    Then I click on FIAT Currency Wallet type

    And I click on British Currency Wallet Type

    And I click on confirm

    And Wallet Already Added error shows up
    And I click on OK
    And I Check Wallet Name as GBP


#CryptoAdvanceWallet
#  Scenario: I verify adding same crypto Advance - bitcoin cash wallet
#
#    When I scroll down to see the bottom of the App
#
#    When I click on + button
#
#    Then I click on Crypto Advance wallet
#
#    Then I add bitcoin cash Advance wallet BCH
#
#    Then I click on confirm
#
#    And Wallet Already Added error shows up
#
#  Scenario: I verify adding same crypto Advance - Litcoin LTC Advance wallet
#
#    When I scroll down to see the bottom of the App
#
#    When I click on + button
#
#    Then I click on Crypto Advance wallet
#
#    Then I add Litcoin LTC Advance wallet
#
#    Then I click on confirm
#    And Wallet Already Added error shows up
#
#  Scenario: I verify adding same crypto Advance - Bitcoin wallet
#
#    When I scroll down to see the bottom of the App
#
#    When I click on + button
#
#    Then I click on Crypto Advance wallet
#
#    Then I add Bitcoin wallet Advance BTC
#
#    Then I click on confirm
#
#    And Wallet Already Added error shows up

#BXCoin
  Scenario: I verify adding BX coin wallet again

    When I scroll down to see the bottom of the App

    When I click on + button

    Then I click on Token

    Then I select wallet currency for BX

    Then I click on confirm

    And Wallet Already Added error shows up

    And I click on OK

    And I Check Wallet Name as Baanx Token



#UI Actions
  Scenario: I verify UI Action to click on the different tab

    When I click on wallet tab

    And  I click on exchange tab

    And I click on CL Cards tab

    And I click on Buy BXX tab

    And I click on setting tab

  Scenario: I verify Exchange option for Buy

    When I click on exchange tab

    And I click on BUY option

    Then I click on Buy BCH graph

    When I click on exchange tab

    Then I click on Buy BX token graph

    When I click on exchange tab

    Then I click on Buy Litecoin graph

    When I click on exchange tab

    When I scroll down to see the bottom of the App

  Scenario: I verify Exchange option for Sell
    When I click on exchange tab

    And I click on SELL option

    When I click on exchange tab

    Then I click on SELL BCH graph

    When I click on exchange tab

    Then I click on SELL Litecoin graph

    When I click on exchange tab

    Then I click on SELL Bitcoin graph

  Scenario: I verify Buy BXX coin option from bar

    When I click on Buy BXX tab

    Then I click on Buy BXX option

    Then Account details information should displayed to transfer in Baanx Account

    Then I click on OK

#AccountLock
  Scenario: I validate the login if Account is locked

    When I provide user name password for locked account

    Then I click on Request SMS

    Then Account Lock message should be displayed

##Validate sending money to another wallet

  Scenario: I verify sending money to Another wallet - from BCH

    When I click on BTC wallet

    Then I click on send Money

    Then I provide Valid recipeint address

    And I provide Amount

    Then I click on screen to remove the keywbord

    Then I click on calculate fee

    Then I click on send Transaction

    Then I click on Request SMS Code


##transaction when do not have enough balance

  Scenario: I verify sending money to Another wallet - When do not have enough balance

    When I click on BTC wallet

    Then I click on send Money

    Then I provide Valid recipeint address

    And I provide Amount

    Then I click on screen to remove the keywbord

    Then I click on calculate fee

    Then I get low balance Error


    ##validate message update for authentication:

  Scenario: Validate Authentication message update - GCC-25

    When I click on BTC wallet

    Then I click on send Money

    Then I provide Valid recipeint address

    And I provide Amount

    Then I click on screen to remove the keywbord

    Then I click on calculate fee

    Then I click on send Transaction

    And I validate Authorization message


  Scenario: Check newly created Added Wallet option

      When I click on newly created Add wallet option

      And I add bitcoin cash wallet BCH

      Then I click on confirm

  Scenario: I validate available wallet types for Baanx App

      When I click on wallet tab

      Then I scroll down to see the bottom of the App

      And I click on + button

      Then I validate for crypto wallet type

      And I validate for FIAT wallet Type

      And I validate for Token wallet type


  Scenario: Validate Authentication message update for BX app  - GCC25

    When I click on BTC wallet

    Then I click on send Money

    Then I provide Valid recipeint address

    And I provide Amount

    Then I click on screen to remove the keywbord

    Then I click on calculate fee

    Then I click on send Transaction

    And I validate Authorization message
