# new feature
# Tags: optional

Feature: Validate baanx app when KYC is not done

  Background: I setup appium for iOS App for User 2

    Given I setup appium for iOS App for Non KYC User

  @Regression
  @Automated
  Scenario: I verify adding new FIAT Currency wallet - USD - KYC Not Done

    When I scroll down to see the bottom of the App

    Then I click on + button

    Then I click on FIAT Currency Wallet type

    And I click on USD currency wallet

    Then I click on confirm

    Then I check if KYC is done for User

  @Regression
  @Automated
  Scenario: I verify adding new FIAT Currency wallet - Euro - KYC Not Done

    Then I scroll down to see the bottom of the App

    Then I click on + button

    Then I click on FIAT Currency Wallet type

    And I click on Euro Currency Wallet

    Then I click on confirm

    Then I check if KYC is done for User

  @Regression
  @Automated
  Scenario: I verify adding new FIAT Currency wallet - British Currency - KYC Not Done

    Then I scroll down to see the bottom of the App

    Then I click on + button

    Then I click on FIAT Currency Wallet type

    And I click on British Currency Wallet Type

    Then I click on confirm

    Then I check if KYC is done for User

  @Regression
  @Automated
  Scenario: I Validate ordering card if KYC is not enabled

    When I click on CL Cards tab - debit card

    Then I click to Order the card

    Then I choose Premium option

    And KYC not enabled error comes

    Then I Accept the error

    And I choose Standard option

    And KYC not enabled error comes

    Then I Accept the error