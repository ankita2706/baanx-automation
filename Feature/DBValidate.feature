# new feature
# Tags: optional

Feature: Validate baanx token Check for ordering the card

  Scenario: I verify ordering of card when BX tokens are more than 5000

    Given I setup appium for iOS App for User 1

    When I click on CL Card for BX

    Then I click to Order the card for BX

    Then I click on Premium for BX

    And I check for BX token

    And I agree on terms and condition for BX

    And I Choose currency as GBP for BX

    And I verify the address for BX

    Then I Validate My name on The Card for BX

    And I see congratulations Message for BX


  Scenario: I verify ordering of card when BX tokens are less than 5000

    Given I setup appium for iOS App for User 2

    When I click on CL Card for BX

    Then I click to Order the card for BX

    Then I click on Premium for BX

    And I check for BX token

    And  I see Low balance error