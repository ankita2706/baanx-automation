Feature: Validate Login for Baanx app

  Background: setup appium

  Given I setup appium for iOS App


# login in APP
  Scenario: I verify login in baanx app for valid user

    When I provide User name and password

    Then I click on Request SMS

    Then SMS sent message should come

  Scenario: I verify login in baanx for invalid user

    When I provide invalid User name and password

    Then Error message should come

#    Logout from app
  Scenario: I logout from baanx app

    When I click on setting tab

    When I scroll down to see the bottom of the App

    Then I click on Sign out