package cucumberTest;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by spuppala on 29/12/16.
 */

@RunWith(Cucumber.class)
@CucumberOptions(monochrome = true,
        glue = "BxAppium",
        features = {
                "Feature"

        },
        dryRun = true,
        plugin = {"com.gainsight.automation.plugins.LoggerPlugin", "com.gainsight.automation.plugins.TestLinkPlugin"
        })
public class TestLinkRunner {


}
