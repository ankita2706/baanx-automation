package StepDefinition;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebElement;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class Steps {
    public static URL url;
    public static DesiredCapabilities capabilities;
    public static IOSDriver<IOSElement> driver;


    @Given("I setup appium for iOS App")
    public void setupAppium() throws MalformedURLException {
        //2
        final String URL_STRING = "http://127.0.0.1:4723/wd/hub";
        url = new URL(URL_STRING);

        //3
        capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone X");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "12.2");
        capabilities.setCapability(MobileCapabilityType.APP, "/Users/ankita/Library/Developer/Xcode/DerivedData/baanx-atvxwfkgmdijqmhhqjynjohuehde/Build/Products/Debug-iphonesimulator/Baanx.app");
        capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
        capabilities.setCapability("useNewWDA", false);
        driver = new IOSDriver<IOSElement>(url, capabilities);
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.resetApp();

    }

    @Given("I setup appium for iOS App for Non KYC User")
    public void setupAppiumforUser2() throws MalformedURLException {
        //2
        final String URL_STRING = "http://127.0.0.1:4723/wd/hub";
        url = new URL(URL_STRING);

        //3
        capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 8");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "12.2");
        capabilities.setCapability(MobileCapabilityType.APP, "/Users/ankita/Library/Developer/Xcode/DerivedData/baanx-atvxwfkgmdijqmhhqjynjohuehde/Build/Products/Debug-iphonesimulator/Baanx.app");
        capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
        capabilities.setCapability("useNewWDA", false);
        driver = new IOSDriver<IOSElement>(url, capabilities);
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.resetApp();

    }

    @Given("I setup appium for iOS GCC App")
    public void setupAppiumGCC() throws MalformedURLException {
        //2
        final String URL_STRING = "http://127.0.0.1:4723/wd/hub";
        url = new URL(URL_STRING);

        //3
        capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone X");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "12.2");
        capabilities.setCapability(MobileCapabilityType.APP, "/Users/ankita/Library/Developer/Xcode/DerivedData/baanx-atvxwfkgmdijqmhhqjynjohuehde/Build/Products/Debug-iphonesimulator/GCC.app");
        capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
        capabilities.setCapability("useNewWDA", false);
        driver = new IOSDriver<IOSElement>(url, capabilities);
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.resetApp();

    }


    @When("I provide User name and password")
    public void iProvideUserNameAndPassword() {
        driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"baanx\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField\n")).sendKeys("Ankita.yadav+2706@baanx.com");
        driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"baanx\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeSecureTextField")).sendKeys("Ankita12345!");
        driver.findElement(By.name("LOGIN")).click();
        //driver.resetApp();
    }

    @Then("I reset the app")
    public void iResetTheApp() {
        driver.removeApp("com.bzytan.appiumeveryday");
    }

    @When("I provide invalid User name and password")
    public void iProvideInvalidUserNameAndPassword() {
        driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"baanx\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField\n")).sendKeys("ankita.yadav@baanx.com");
        driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"baanx\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeSecureTextField")).sendKeys("Ankita12345!");
        driver.findElement(By.name("LOGIN")).click();
        //driver.resetApp();
    }

    @Then("Error message should come")
    public void errorMessageShouldCome() {
        if (driver.getPageSource().contains("Invalid Login deatils")) ;
        {
            System.out.println("Invalid Login Details");
        }
    }

    @Then("SMS sent message should come")
    public void smsSentMessageShouldCome() {


        if (driver.getPageSource().contains("A SMS has been sent")) {
            System.out.println("SMS Sent");

        } else {
            driver.findElement(By.name("X")).click();

        }

    }


    @Then("I click on Request SMS")
    public void iClickOnRequestSMS() {
        driver.findElement(By.name("Request SMS")).click();

    }

    @When("I click on + button")
    public void iClickOnButton() {
        driver.findElement(By.name("+")).click();
    }


    @Then("I add bitcoin cash wallet BCH")
    public void iAddBitcoinCashWalletBCH() {
        driver.findElement(By.name("Cryptocurrencies")).click();
        driver.findElement(By.name("Bitcoin Cash (BCH)")).click();
    }

    @Then("I click on confirm")
    public void iClickOnConfirm() {
        driver.findElement(By.name("Confirm")).click();

    }

    @Then("I add Litcoin LTC wallet")
    public void iAddLitcoinLTCWallet() {

        driver.findElement(By.name("Cryptocurrencies")).click();
        driver.findElement(By.name("Litecoin (LTC)")).click();
    }


    @Then("I add Bitcoin wallet BTC")
    public void iAddBitcoinWalletBTC() {
        driver.findElement(By.name("Cryptocurrencies")).click();
        driver.findElement(By.name("Bitcoin (BTC)")).click();
    }

    @Then("I click on Token")
    public void iClickOnToken() {
        driver.findElement(By.name("Token")).click();
    }

    @Then("I select wallet currency for BX")
    public void iSelectWalletCurrencyForBX() {
        driver.findElement(By.name("Baanx Token (BXX)")).click();

    }

    @When("I click on wallet tab")
    public void iClickOnWalletTab() {
        driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Wallets\"]")).click();
    }

    @And("I click on exchange tab")
    public void iClickOnExchangeTab() {
        driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Exchange\"]")).click();

    }

    @And("I click on setting tab")
    public void iClickOnSettingTab() {
        driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Settings\"]")).click();

    }

    @And("I click on CL Cards tab")
    public void iClickOnPayTab() {
        if (driver.getPageSource().contains("CL Card")) {
            driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"CL Card\"]")).click();
        } else {
            driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Pay\"]")).click();
        }
    }

    @And("I click on Buy BXX tab")
    public void iClickOnBuyBXXTab() {
        driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Buy BXX\"]")).click();

    }

    @Then("I click on Buy BCH graph")
    public void iClickOnBuyBCHGraph() {
        driver.findElement(By.xpath("(//XCUIElementTypeOther[@name=\"Line Chart. 1 dataset. Number\"])[1]")).click();

    }

    @Then("I click on Buy BX token graph")
    public void iClickOnBuyBXTokenGraph() {
        driver.findElement(By.xpath("(//XCUIElementTypeOther[@name=\"Line Chart. 1 dataset. Number\"])[2]")).click();

    }

    @Then("I click on Buy Litecoin graph")
    public void iClickOnBuyLitecoinGraph() {
        driver.findElement(By.xpath("(//XCUIElementTypeOther[@name=\"Line Chart. 1 dataset. Number\"])[3]")).click();

    }

    @Then("I click on Buy Bitcoin graph")
    public void iClickOnBuyBitcoinGraph() {
        driver.findElement(By.xpath("((//XCUIElementTypeOther[@name=\"Line Chart. 1 dataset. Number\"])[4]")).click();

    }

    @And("I click on BUY option")
    public void iClickOnBUYOption() {
        driver.findElement(By.name("BUY")).click();
    }

    @And("I click on SELL option")
    public void iClickOnSELLOption() {
        driver.findElement(By.name("SELL")).click();

    }

    @Then("I click on SELL BCH graph")
    public void iClickOnSELLBCHGraph() {
        driver.findElement(By.xpath("(//XCUIElementTypeOther[@name=\"Line Chart. 1 dataset. Number\"])[1]")).click();

    }

    @Then("I click on SELL Litecoin graph")
    public void iClickOnSELLLitecoinGraph() {
        driver.findElement(By.xpath("(//XCUIElementTypeOther[@name=\"Line Chart. 1 dataset. Number\"])[2]")).click();

    }

    @Then("I click on SELL Bitcoin graph")
    public void iClickOnSELLBitcoinGraph() {
        driver.findElement(By.xpath("(//XCUIElementTypeOther[@name=\"Line Chart. 1 dataset. Number\"])[3]")).click();

    }

    @Then("Account details information should displayed to transfer in Baanx Account")
    public void Account_details_information_should_come_to_transfer_in_Baanx_Account() {
        System.out.println("To buy your BXX tokens please send your desired amount via bank transfer to: Bank: Baanx Group Ltd Account: 52180131 Sort Code: 54-41-47 IBAN: GB40 NWBK 5441 4752 1801 31 BIC: NWBKGB2L Using the following payment reference: ANKITAAND-BXX");

    }

    @Then("Account Lock message should be displayed")
    public void Account_Lockmessage_should() {
        if (driver.getPageSource().contains("Account is locked")) ;
        {
            System.out.println("Account is Locked");
        }

    }

    @Then("I click on Buy BXX option")
    public void I_click_on_Buy_BXX_option() {
        driver.findElement(By.xpath("(//XCUIElementTypeButton[@name=\"Buy BXX\"])[1]")).click();
    }

    @Then("I click on Sign out")
    public void Sign_Out() {
        driver.findElement(By.name("Sign out")).click();
    }

    @Then("I provide user name password for locked account")
    public void locked_Account() {
        driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"baanx\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField\n")).sendKeys("ankita.yadav+99@baanx.com");
        driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"baanx\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeSecureTextField")).sendKeys("Ankita12345!");
        driver.findElement(By.name("LOGIN")).click();

    }

    @When("I scroll down to see the bottom of the App")
    public void iScrollDownToSeeTheBottomOfTheApp() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        HashMap<String, String> scrollObject = new HashMap<String, String>();
        scrollObject.put("direction", "down");
        js.executeScript("mobile: scroll", scrollObject);
    }

    @Then("I click on OK")
    public void iClickOnOK() {
        driver.findElement(By.name("OK")).click();
    }

    @Then("I click on FIAT Currency Wallet type")
    public void iClickOnFIATCurrencyWalletType() {
        driver.findElement(By.name("Global (Fiat) Currencies")).click();

    }

    @And("I click on USD currency wallet")
    public void iClickOnUSDCurrencyWallet() {
        driver.findElement(By.name("US Dollar (USD)")).click();

    }


    @And("I click on Euro Currency Wallet")
    public void iClickOnEuroCurrencyWallet() {


        driver.findElement(By.name("Euro (EUR)")).click();
    }


    @And("I click on British Currency Wallet Type")
    public void iClickOnBritishCurrencyWalletType() {

        driver.findElement(By.name("British Pound (GBP)")).click();
    }

    @Then("Error message should be displayed")
    public void ShowErrorMessage() {
        System.out.println("cannot add");
    }

    @Then("I click on Crypto Advance wallet")
    public void iClickOnCryptoAdvanceWallet() {
        driver.findElement(By.name("Crypto Advance")).click();

    }

    @Then("I add bitcoin cash Advance wallet BCH")
    public void iAddBitcoinCashAdvanceWalletBCH() {
        driver.findElement(By.name("Bitcoin Cash (BCH Advance)")).click();

    }

    @Then("I add Litcoin LTC Advance wallet")
    public void iAddLitcoinLTCAdvanceWallet() {

        driver.findElement(By.name("Litecoin (LTC Advance)")).click();

    }

    @Then("I add Bitcoin wallet Advance BTC")
    public void iAddBitcoinWalletAdvanceBTC() {
        driver.findElement(By.name("Bitcoin (BTC Advance)")).click();
    }


    @When("I click on BTC wallet")
    public void iClickOnBCHWallet() {
        driver.findElement(By.name("BTC")).click();
    }

    @Then("I click on send Money")
    public void iClickOnSendMoney() {
        driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Send\"]\n")).click();

    }


    @Then("I provide Valid recipeint address")
    public void iProvideValidRecipeintAddress() {
        driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"baanx\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeTextField[1]\n")).sendKeys("XYZ");

    }

    @And("I provide Amount")
    public void iProvideAmount() {
        driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"baanx\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeTextField[2]\n")).sendKeys(".00001");
    }

    @Then("I click on calculate fee")
    public void iClickOnCalculateFee() {
        driver.findElement(By.name("CALCULATE FEES")).click();

    }

    @Then("I click on screen to remove the keywbord")
    public void iClickOnScreenToRemoveTheKeywbord() {
        driver.findElement(By.name("Recipient")).click();
    }

    @Then("I click on send Transaction")
    public void iClickOnSendTransaction() {
        driver.findElement(By.name("SEND TRANSACTION")).click();
    }

    @Then("I click on Request SMS Code")
    public void iClickOnRequestSMSCode() {
        driver.findElement(By.name("Request SMS Code")).click();
    }

    @Then("I verify PopUp")
    public void iVerifyPopUp() {
        Alert alert = driver.switchTo().alert();
        alert.accept();

    }


    @Then("I add the wallet")
    public void iAddTheWallet() {
    }

    @Then("I check if Eur wallet is already present")
    public void iCheckIfWalletIsAlreadyPresent() {

        int EurWallet = 0;
        if (driver.findElement(By.name("Ankita.Yadav@Baanx.Com's EUR Wallet")) != null) {
            EurWallet++;
        } else {
            EurWallet = 0;
        }

        if (EurWallet > 0) {
            System.out.println("Wallet is already added");
        } else {
            driver.findElement(By.name("Ankita.Yadav@Baanx.Com's EUR Wallet")).click();

        }
    }

    @Then("I show error message")
    public void iShowErrorMessage() {
        System.out.println("Wallet is already added");
    }


    @Then("I check if USD wallet is already present")
    public void iCheckIfUSDWalletIsAlreadyPresent() {

        int USDWallet = 0;
        if (driver.findElement(By.name("Ankita.Yadav@Baanx.Com's USD Wallet")) != null) {
            USDWallet++;
        } else {
            USDWallet = 0;
        }

        if (USDWallet > 0) {
            System.out.println("Wallet is already added");
        }
    }

    @And("Wallet Already Added error shows up")

    public void walletAlreadyAddedErrorShowsUp() {
        {
            if (driver.getPageSource().contains("You already have a wallet for")) {
                System.out.println("Error message");
            } else {
                driver.findElement(By.name("X")).click();

            }


        }
    }

    @Then("I check if KYC is done for User")
    public void iCheckIfKYCIsDoneForUser() {
        if (driver.getPageSource().contains("KYC Required")) {
            driver.findElement(By.name("Not Yet")).click();
        } else {
            driver.findElement(By.name("confirm")).click();

        }
    }

    @Then("I get low balance Error")
    public void iGetLowBalanceError() {

        if (driver.getPageSource().contains("You Do not have")) {
            System.out.println("Low Balance error");
        }
    }

    @And("I validate Authorization message")
    public void iValidateAuthorizationMessage() {

        if (driver.getPageSource().contains("Enter Two Factor Authentication Code")) {
            System.out.println("Enter Two Factor Authentication Code");

        } else {
            driver.findElement(By.name("confirm")).click();

        }
    }

    @Then("I click on GCC Tokens")
    public void iClickOnGCCTokens() {
        driver.findElement(By.name("Tokens")).click();

    }

    @Then("I select wallet currency for GCC Token 100")
    public void iSelectWalletCurrencyForGCCToken100() {
        driver.findElement(By.name("Token 100")).click();
    }

    @Then("I select wallet currency for GCC Token 111")
    public void iSelectWalletCurrencyForGCCToken111() {
        driver.findElement(By.name("Token 111")).click();
    }

    @Then("I select wallet currency for GCC Token 561")
    public void iSelectWalletCurrencyForGCCToken561() {
        driver.findElement(By.name("Token 561")).click();

    }

    @And("I click on Invest tab")
    public void iClickOnInvestTab() {
        driver.findElement(By.name("Invest")).click();

    }

    @And("Error message should come for KYC")
    public void errorMessageShouldComeForKYC() {
        if (driver.getPageSource().contains("KYC Required")) {

            System.out.println("KYC Not DOne");
        } else {
            System.out.println("KYC Not DOne message not done");
        }


    }

    @Then("I check I check if KYC is enabled")
    public void iCheckICheckIfKYCIsEnabled() {
        if (driver.getPageSource().contains("Verify Account Unverified")) {

            System.out.println("tillu");
        } else {
            System.out.println("KYC Not DOne message not done");
        }


    }


    @When("I click on newly created Add wallet option")
    public void iClickOnNewlyCreatedAddWalletOption() {
        driver.findElement(By.name("ADD WALLET")).click();
    }


    @Then("I validate for crypto wallet type")
    public void iValidateForCryptoWalletType() {
        if(driver.getPageSource().contains("Cryptocurrencies"))
        {System.out.println("Cryptocurrienies Wallet");}

else {
    driver.findElement(By.name("X")).click();
        }
    }

    @And("I validate for FIAT wallet Type")
    public void iValidateForFIATWalletType() {
        if(driver.getPageSource().contains("Global (Fiat) Currencies"))
        {System.out.println("Fiat (Global) Currencies Wallet");}

        else {
            driver.findElement(By.name("X")).click();
        }
    }

    @And("I validate for Token wallet type")
    public void iValidateForTokenWalletType() {
        if(driver.getPageSource().contains("Token"))
        {System.out.println("Token Wallet");}

        else {
            driver.findElement(By.name("X")).click();
        }
    }

    @Then("I Check Wallet Name as BCH")
    public void iCheckWalletNameAsBCH() {
        RemoteWebElement element = (RemoteWebElement)driver. findElement(By.name("BCH"));
        String elementID = element.getId();
        HashMap<String, String> scrollObject = new HashMap<String, String>();
        scrollObject.put("element", elementID);
        scrollObject.put("toVisible", "BCH");
        driver.executeScript("mobile:scroll", scrollObject);
        driver.findElement(By.name("BCH")).click();


    }


    @Then("I Check Wallet Name as LTC")
    public void iCheckWalletNameAsLTC() {
        RemoteWebElement element = (RemoteWebElement)driver. findElement(By.name("LTC"));
        String elementID = element.getId();
        HashMap<String, String> scrollObject = new HashMap<String, String>();
        scrollObject.put("element", elementID);
        scrollObject.put("toVisible", "LTC");
        driver.executeScript("mobile:scroll", scrollObject);
        driver.findElement(By.name("LTC")).click();

    }

    @Then("I Check Wallet Name as BTC")
    public void iCheckWalletNameAsBTC() {
        RemoteWebElement element = (RemoteWebElement)driver. findElement(By.name("BTC"));
        String elementID = element.getId();
        HashMap<String, String> scrollObject = new HashMap<String, String>();
        scrollObject.put("element", elementID);
        scrollObject.put("toVisible", "BTC");
        driver.executeScript("mobile:scroll", scrollObject);
        driver.findElement(By.name("BTC")).click();

    }

    @Then("I Check Wallet Name as USD")
    public void iCheckWalletNameAsUSD() {
        RemoteWebElement element = (RemoteWebElement)driver. findElement(By.name("USD"));
        String elementID = element.getId();
        HashMap<String, String> scrollObject = new HashMap<String, String>();
        scrollObject.put("element", elementID);
        scrollObject.put("toVisible", "USD");
        driver.executeScript("mobile:scroll", scrollObject);
        driver.findElement(By.name("USD")).click();

    }

    @Then("I Check Wallet Name as EUR")
    public void iCheckWalletNameAsEUR() {
        RemoteWebElement element = (RemoteWebElement)driver. findElement(By.name("EUR"));
        String elementID = element.getId();
        HashMap<String, String> scrollObject = new HashMap<String, String>();
        scrollObject.put("element", elementID);
        scrollObject.put("toVisible", "EUR");
        driver.executeScript("mobile:scroll", scrollObject);
        driver.findElement(By.name("EUR")).click();

    }

    @Then("I Check Wallet Name as GBP")
    public void iCheckWalletNameAsGBP() {
        RemoteWebElement element = (RemoteWebElement)driver. findElement(By.name("GBP"));
        String elementID = element.getId();
        HashMap<String, String> scrollObject = new HashMap<String, String>();
        scrollObject.put("element", elementID);
        scrollObject.put("toVisible", "GBP");
        driver.executeScript("mobile:scroll", scrollObject);
        driver.findElement(By.name("GBP")).click();

    }

    @Then("I Check Wallet Name as Baanx Token")
    public void iCheckWalletNameAsBaanxToken() {
        RemoteWebElement element = (RemoteWebElement)driver. findElement(By.name("Baanx Token"));
        String elementID = element.getId();
        HashMap<String, String> scrollObject = new HashMap<String, String>();
        scrollObject.put("element", elementID);
        scrollObject.put("toVisible", "Baanx Token");
        driver.executeScript("mobile:scroll", scrollObject);
        driver.findElement(By.name("Baanx Token")).click();
    }



    //debit card
    @Then("I click to Order the card")
    public void I_click_to_Order_the_card(){
        //driver.findElement(By.name("Order your card")).click();
        driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Order your card\"]")).click();
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;


    }

    @Then("I choose Premium option")
    public void iChoosePremiumOption() {

        driver.manage().timeouts().implicitlyWait(200,TimeUnit.SECONDS) ;
        driver.findElement(By.name("Premium")).click();


    }

    @And("I agree on terms and condition")
    public void iAgreeOnTermsAndCondition() {
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
        driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"baanx\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeSwitch\n")).click();
        driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Continue\"]\n")).click();
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
    }

    @Then("I continue")
    public void iContinue() {
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
        driver.findElement(By.name("Continue")).click();
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;

    }

    @And("I verify the address")
    public void iVerifyTheAddress() {
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
        driver.findElement(By.name("Continue")).click();
        driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Continue\"]\n")).click();
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
    }

    @Then("I Validate My name on The Card")
    public void iValidateMyNameOnTheCard() {
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
        System.out.println("I Validate My name on The Card");
        driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Continue\"]\n")).click();
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;

    }

    @Then("I review my Details")
    public void iReviewMyDetails() {
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
        System.out.println("I review my Details");
        driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Continue\"]\n")).click();
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;

    }

    @And("I see congratulations Message")
    public void iSeeCongratulationsMessage() {
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
        System.out.println("I see congratulations Message");
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
    }

    @When("I click on CL Cards tab - debit card")
    public void iClickOnCLCardsTabDebitCard() {

        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
        driver.findElement(By.name("CL Card")).click();
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
    }


    @And("I Choose currency as GBP")
    public void iChooseCurrencyAsGBP() {
        driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"baanx\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[3]\n")).click();
        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS) ;
    }

    @And("I exit the screen")
    public void iExitTheScreen() {

        driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"baanx\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther\n")).click();
    }

    @When("I click on Activate the card")
    public void iClickOnActivateTheCard() {
        driver.findElement(By.name("Activate Card")).click();
    }

    @Then("I provide Valid CVV number")
    public void iProvideValidCVVNumber() {

        driver.findElement(By.xpath("")).sendKeys("");
    }


    @And("Card should be Activated")
    public void cardShouldBeActivated() {
        driver.findElement(By.name("Start using card")).click();
    }

    @Then("I provide InValid CVV number")
    public void iProvideInValidCVVNumber() {

        driver.findElement(By.xpath("")).sendKeys("");
    }

    @And("Wrong CVV message should be displayed")
    public void wrongCVVMessageShouldBeDisplayed() {
        System.out.println("Wrong CVV");

    }

    @And("Debit card Lock Message should be deisplayed")
    public void debitCardLockMessageShouldBeDeisplayed() {
        System.out.println("Debit card locked");

    }

    @Then("I provide spacial characters in CVV number")
    public void iProvideSpacialCharactersInCVVNumber() {
        driver.findElement(By.xpath("")).sendKeys("");
    }

    @Then("I click on Confirm card Number")
    public void iClickOnConfirmCardNumber() {

        driver.findElement(By.name("Confirm Card Number")).click();
    }



    @And("It show error message for blank CVV")
    public void itShowErrorMessageForBlankCVV() {

        if(driver.getPageSource().contains("Cannot activate card without CVV")){
            System.out.println("Correct Message");
        }
        else{
            driver.findElement(By.name("X")).click();
        }
    }
    @Then("I see sucess Message for card Activation")
    public void iSeeSucessMessageForCardActivation() {
        {

            if(driver.getPageSource().contains("Yor card is now Ready to use")){
                System.out.println("Correct Message");
            }
            else{
                driver.findElement(By.name("X")).click();
            }
        }
    }

    @Then("I choose Standard option")
    public void iChooseStandardOption() {
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
        driver.findElement(By.name("Standard")).click();
    }


    @Then("I Accept the error")
    public void iAcceptTheError() {
        driver.findElement(By.name("OK")).click();
    }

    @And("KYC not enabled error comes")
    public void kycNotEnabledErrorComes() {
        if(driver.getPageSource().contains("KYC"))
        {
            System.out.println("You must complete the account verification process before you can order your card.");
        }

        else {
            driver.findElement(By.name("X")).click();
        }
    }

    @And("I see insuffient BXX token error")
    public void iSeeInsuffientBXXTokenError() {
        if(driver.getPageSource().contains("KYC"))
        {
            System.out.println("BXX Error");
        }

        else {
            driver.findElement(By.name("X")).click();
        }
    }
}




