/*
* Created 2 different instances to validate BX token
* for user1 , there is more than 5000 BX tokens present
* for User 2, less tokens are present
* */

package StepDefinition;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.*;
import java.util.concurrent.TimeUnit;

public class DBSteps {

    public static URL url;
    public static DesiredCapabilities capabilities;
    public static IOSDriver<IOSElement> driver;


    @Given("I setup appium for iOS App for User 1")
    public void setupAppiumforUser1() throws MalformedURLException {
        //2
        final String URL_STRING = "http://127.0.0.1:4723/wd/hub";
        url = new URL(URL_STRING);

        //3
        capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone X");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "12.2");
        capabilities.setCapability(MobileCapabilityType.APP, "/Users/ankita/Library/Developer/Xcode/DerivedData/baanx-atvxwfkgmdijqmhhqjynjohuehde/Build/Products/Debug-iphonesimulator/Baanx.app");
        capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
        capabilities.setCapability("useNewWDA", false);
        driver = new IOSDriver<IOSElement>(url, capabilities);
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.resetApp();

    }


    @Given("I setup appium for iOS App for User 2")
    public void setupAppiumforUser2() throws MalformedURLException {
        //2
        final String URL_STRING = "http://127.0.0.1:4723/wd/hub";
        url = new URL(URL_STRING);

        //3
        capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 8");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "12.2");
        capabilities.setCapability(MobileCapabilityType.APP, "/Users/ankita/Library/Developer/Xcode/DerivedData/baanx-atvxwfkgmdijqmhhqjynjohuehde/Build/Products/Debug-iphonesimulator/Baanx.app");
        capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
        capabilities.setCapability("useNewWDA", false);
        driver = new IOSDriver<IOSElement>(url, capabilities);
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.resetApp();

    }

    @Given("I check for BX token")
    public static void BXToken() throws ClassNotFoundException, SQLException {


        //Connection URL Syntax: "jdbc:mysql://ipaddress:portnumber/db_name"
        Class.forName("org.postgresql.Driver");
        String dbUrl = "jdbc:postgresql://baanx-node-db.ckp0gfznkyth.eu-west-1.rds.amazonaws.com:5432/baanxdb";
        String username = "ankita";
        String password = "ankitacanseeproductiondata";
        String query = "select confirmed_balance from user_addresses\n" +
                "where user_id = 'c85c51fe-f50d-4fac-b440-537bab411193'\n" +
                "and confirmed_balance >= 50000000000\n" +
                "and wallet_type =  'internal'";
        Class.forName("com.mysql.jdbc.Driver");
        Connection con = DriverManager.getConnection(dbUrl, username, password);
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            String balance = rs.getString(1);
            String str1 = String.valueOf(balance);
            String str2 = String.valueOf(4999);
            System.out.println("balance is" + " " + balance);
            System.out.println("str" + " " + str1);
            System.out.println("str2" + " " + str2);
            int compare = str1.compareToIgnoreCase(str2);
            if (compare < 0) {
                driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"baanx\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeSwitch\n")).click();
                driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Continue\"]\n")).click();
            } else if (compare > 0) {
                System.out.println("Low BXX Token");
            }


        }

        // closing DB Connection

    }

    @When("I click on CL Card for BX")
    public void iClickOnCLCardForBX() {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.findElement(By.name("CL Card")).click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    @Then("I click to Order the card for BX")
    public void I_click_to_Order_the_card() {
        //driver.findElement(By.name("Order your card")).click();
        driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Order your card\"]")).click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    @Then("I click on Premium for BX")
    public void iClickOnPremium() {

        driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);
        driver.findElement(By.name("Premium")).click();
    }

    @And("I agree on terms and condition for BX")
    public void iAgreeOnTermsAndConditionForBX() {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"baanx\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeSwitch\n")).click();
        driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Continue\"]\n")).click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

    }

    @And("I Choose currency as GBP for BX")
    public void iChooseCurrencyAsGBPForBX() {

        driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"baanx\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[3]\n")).click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @And("I verify the address for BX")
    public void iVerifyTheAddressForBX() {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.findElement(By.name("Continue")).click();
        driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Continue\"]\n")).click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

    }

    @Then("I Validate My name on The Card for BX")
    public void iValidateMyNameOnTheCardForBX() {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        System.out.println("I Validate My name on The Card");
        driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Continue\"]\n")).click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    @And("I see congratulations Message for BX")
    public void iSeeCongratulationsMessageForBX() {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        System.out.println("I see congratulations Message");
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    @And("I see Low balance error")
    public void iSeeLowBalanceError() {
        if(driver.getPageSource().contains("You must have 5000 BXX in your BXX wallet to purchase this card")){
            System.out.println("You must have 5000 BXX in your BXX wallet to purchase this card.");
        }
        else {
            driver.findElement(By.name("X")).click();
        }
    }

}


