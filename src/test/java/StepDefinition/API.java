package StepDefinition;

import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class API {


    public static URL url;
    public static DesiredCapabilities capabilities;
    public static IOSDriver<IOSElement> driver;

    public void setupAppium() throws MalformedURLException {
        //2
        final String URL_STRING = "http://127.0.0.1:4723/wd/hub";
        url = new URL(URL_STRING);

        //3
        capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone X");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "12.2");
        capabilities.setCapability(MobileCapabilityType.APP, "/Users/ankita/Library/Developer/Xcode/DerivedData/baanx-atvxwfkgmdijqmhhqjynjohuehde/Build/Products/Debug-iphonesimulator/baanx.app");
        capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
        capabilities.setCapability("useNewWDA", false);
        driver = new IOSDriver<IOSElement>(url, capabilities);
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.resetApp();

    }


    @Test
    public void apitest() throws IOException, ParseException {


        URL url = new URL("https://secure.baanx.co.uk/api/v1/market_rates");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.connect();
        int responsecode = conn.getResponseCode();
        String inline = "";
        if (responsecode != 200) throw new RuntimeException("HttpResponseCode:" + responsecode);
        else {

            Scanner sc = new Scanner(url.openStream());

            while (sc.hasNext()) {
                inline += sc.nextLine();

            }
            System.out.println("\n Json Data string format");

            System.out.println(inline);
            sc.close();


        }


        JSONParser parse = new JSONParser();
        JSONObject jobj = (JSONObject)parse.parse(inline);
        JSONArray jsonarr_1 = (JSONArray) jobj.get("results");

        for(int i=0;i<jsonarr_1.size();i++)
        {
//Store the JSON objects in an array
//Get the index of the JSON object and print the values as per the index
            JSONObject jsonobj_1 = (JSONObject)jsonarr_1.get(i);
            System.out.println("Elements under results array");
            System.out.println("\nPlace id: " +jsonobj_1.get("place_id"));
            System.out.println("Types: " +jsonobj_1.get("types"));
        }







//
//        for (int i = 0; i<arr_1.size(); i++){
//
//            JSONObject jobj_1 = (JSONObject)arr_1.get(i);
//            System.out.println("elelmets under result array");
//            System.out.println("\nPlaceId:"+jobj_1.get("place_id"));
//            System.out.println("Types:"+jobj_1.get("Types"));
//
//
//
//        }

    }


    @Test(enabled = true)

    public static Map<String, String> getQueryMap(String query) {
        String[] params = query.split("&");
        Map<String, String> map = new HashMap<String, String>();
        for (String param : params) {
            String name = param.split("=")[0];
            String value = param.split("=")[1];
            map.put(name, value);
        }
        return map;
    }


    @Test
    public void bodyupdate() throws UnirestException {


        String searchQueryApi = "https://secure.baanx.co.uk/api/v1/market_rate";

        JsonNode body = Unirest.get(searchQueryApi)
                .asJson()
                .getBody();
        System.out.println(body);         // gives the full json response
        //System.out.println(body.length);


    }

}
