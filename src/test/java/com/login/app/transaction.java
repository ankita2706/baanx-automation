package com.login.app;

import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;



public class transaction {
    public static URL url;
    public static DesiredCapabilities capabilities;
    public static IOSDriver<IOSElement> driver;

    //1
    @BeforeSuite
    public void setupAppium() throws MalformedURLException {
        //2
        final String URL_STRING = "http://127.0.0.1:4723/wd/hub";
        url = new URL(URL_STRING);

        //3
        capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone Simulator");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "12.2");
        capabilities.setCapability(MobileCapabilityType.APP, "/Users/ankita/Library/Developer/Xcode/DerivedData/baanx-atvxwfkgmdijqmhhqjynjohuehde/Build/Products/Debug-iphonesimulator/baanx.app");
        capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
        capabilities.setCapability("useNewWDA", false);
        driver = new IOSDriver<IOSElement>(url, capabilities);
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.resetApp();

    }

    //5
    @AfterSuite
    public void uninstallApp() throws InterruptedException {
        driver.removeApp("com.bzytan.appiumeveryday");
    }

    //6
    @Test(enabled = true)
    public void UIAction() throws InterruptedException {
        //click on UI elements
        driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Exchange\"]")).click();

    }
    @Test(enabled = true)
    public void exchangeBuy() throws InterruptedException {
        //Exchange
        //Buy
        driver.findElement(By.name("BUY")).click();
        //BTCH
        driver.findElement(By.xpath("(//XCUIElementTypeOther[@name=\"Line Chart. 1 dataset. Number\"])[1]")).click();




        // BaanxToken
        driver.findElement(By.xpath("(//XCUIElementTypeOther[@name=\"Line Chart. 1 dataset. Number\"])[2]")).click();
        //LTC
        driver.findElement(By.xpath("(//XCUIElementTypeOther[@name=\"Line Chart. 1 dataset. Number\"])[3]")).click();
        //BTC
        driver.findElement(By.xpath("((//XCUIElementTypeOther[@name=\"Line Chart. 1 dataset. Number\"])[4]")).click();

    }

    @Test(enabled = true)
    public void exchangeSell() throws InterruptedException {
        //SELL
        driver.findElement(By.name("SELL")).click();
        //BTCH
        driver.findElement(By.xpath("(//XCUIElementTypeOther[@name=\"Line Chart. 1 dataset. Number\"])[1]")).click();
        //LTC
        driver.findElement(By.xpath("(//XCUIElementTypeOther[@name=\"Line Chart. 1 dataset. Number\"])[2]")).click();
        //BTC
        driver.findElement(By.xpath("((//XCUIElementTypeOther[@name=\"Line Chart. 1 dataset. Number\"])[3]")).click();

    }

    @Test(enabled = true)
    public void BuyBxx() throws InterruptedException {
        //BUYBXX
        driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Buy BXX\"]")).click();
        driver.findElement(By.name("BUY BXX HERE ")).click();
        driver.findElement(By.name("OK")).click();

    }


}