package com.login.app;
import java.sql.*;

public class loadData {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        //Connection URL Syntax: "jdbc:mysql://ipaddress:portnumber/db_name"
        String dbUrl = "jdbc:mysql://127.0.0.1:3306/emp?useUnicode=true&characterEncoding=utf8";

        //Database Username
        String username = "root";

        //Database Password
        String password = "password";

        //Load mysql jdbc driver
        Class.forName("com.mysql.jdbc.Driver");

        //Create Connection to DB
        Connection con = DriverManager.getConnection(dbUrl, username, password);

        //Create Statement Object
        Statement stmt = con.createStatement();
        stmt.executeUpdate("INSERT INTO employee " + "VALUES ('Ankita', 29, 4)");

        //Query to Execute
        String query = "select * from employee;";

        // Execute the SQL Query. Store results in ResultSet
        ResultSet rs = stmt.executeQuery(query);

        // While Loop to iterate through all data and print results
        while (rs.next()) {
            String myName = rs.getString(1);
            String myAge = rs.getString(2);
            String myid = rs.getString(3);

            System.out.println(myName + "  " + myAge + " "+ myid );
        }
        // closing DB Connection
        con.close();
    }
}
