package com.android.app;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;


public class loginAndroid {
    public static URL url;
    public static DesiredCapabilities capabilities;
    public static AndroidDriver<AndroidElement> driver;

    //1
    @BeforeSuite
    public void setupAppiumAndroid() throws MalformedURLException {
        //2
        final String URL_STRING = "http://127.0.0.1:4723/wd/hub";
        url = new URL(URL_STRING);

        //3
        capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Emulator");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        //capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "0");
        capabilities.setCapability(MobileCapabilityType.APP, "/Users/ankita/Downloads/baanx_android_1.0(1).apk");
        capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
        /* capabilities.setCapability("useNewWDA", false); */
        driver = new AndroidDriver<AndroidElement>(url, capabilities);
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.resetApp();

    }


    //5
    @AfterSuite
    public void uninstallApp() throws InterruptedException {
        driver.removeApp("com.bzytan.appiumeveryday");
    }

    //6
    @Test(enabled = true, priority = 1, testName = "Validate Login")
    public void CreateAccountAndroid() throws InterruptedException {
        driver.findElement(By.id("com.baanx.baanxapp:id/createAccountButton")).click();
        driver.findElement(By.id("com.baanx.baanxapp:id/firstNameTextField")).sendKeys("Ankita");
        driver.findElement(By.id("com.baanx.baanxapp:id/lastNameTextField")).sendKeys("Android");
        driver.findElement(By.id("com.baanx.baanxapp:id/nextButton")).click();
        driver.findElement(By.id("com.baanx.baanxapp:id/EmailTextField")).sendKeys("ankita.yadav+95@baanx.com");
        driver.findElement(By.id("com.baanx.baanxapp:id/ConfirmEmailTextField")).sendKeys("ankita.yadav+95@baanx.com");
        driver.findElement(By.id("com.baanx.baanxapp:id/nextButton")).click();


        //driver.resetApp();


        //for androiud

    }

}
